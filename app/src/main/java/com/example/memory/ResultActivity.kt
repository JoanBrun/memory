package com.example.memory

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class ResultActivity : AppCompatActivity() {
    lateinit var rollPlayAgain: Button
    lateinit var rollMenu: Button
    lateinit var textMovi: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.memoryball_result)
        actionBar?.hide()
        supportActionBar?.hide()
        val bundle:Bundle? = intent.extras
        rollPlayAgain = findViewById(R.id.play_again)
        rollMenu = findViewById(R.id.menu)
        textMovi = findViewById(R.id.puntos)

        val mode = bundle?.getString("mode")
        val punts = bundle?.getInt("punts")

        textMovi.text = "Punts: $punts"


        rollPlayAgain.setOnClickListener{
            if (mode == "Easy Mode"){
                val intent = Intent(this, EasyGameActivity::class.java)
                startActivity(intent)
            }
            else{
                val intent = Intent(this, DifGameActivity::class.java)
                startActivity(intent)
            }
        }
        rollMenu.setOnClickListener{
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }
    }
}