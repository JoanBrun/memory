package com.example.memory

import android.content.Intent
import android.widget.Chronometer
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.ViewModel

class GameEasyViewModel : ViewModel() {

    val images = arrayOf(
        R.drawable.bola9,
        R.drawable.bola7,
        R.drawable.bola10,
        R.drawable.bola9,
        R.drawable.bola7,
        R.drawable.bola10
    )

    var cartes = mutableListOf<Carta>()
    var moviment = 0
    val cartaGirada = ArrayList<Carta>()
    var correcte = 0
    var moviments = 0

    // Aquesta funció es estàndard de cada classe, i és com el constructor

    init {
        setDataModel()
    }

    // Funció que barreja les imatges de les cartes i crea l'array de Cartes
    private fun setDataModel() {
        images.shuffle()
        for (i in 0..5) {
            cartes.add(Carta(i, images[i]))
        }
    }

    // Funció que comprova l'estat de la carta, el canvia i envia
    // a la vista la imatge que s'ha de pintar

    fun girarCarta(idCarta: Int) {
        if (!cartes[idCarta].flipped){
            cartes[idCarta].flipped = true
            cartaGirada.add(cartes[idCarta])
            moviment++
        }
    }


    // Funció que retorna l'estat actual d'una carta
    fun estatCarta(idCarta: Int): Int {
        if(cartes[idCarta].flipped){
            return cartes[idCarta].resId
        }
        else{
            return R.drawable.carta
        }
    }

    fun moviment2(){
        if (moviment == 2){
            moviment = 0;
            comparar(cartaGirada)
            moviments ++
        }
    }

    fun comparar(cartaGirada: ArrayList<Carta>){
        if (cartaGirada[0].resId == cartaGirada[1].resId){
            correcte ++
        }
        else{
            cartaGirada[0].flipped = false
            cartaGirada[1].flipped = false
        }
        cartaGirada.clear()
    }

    fun correctesReturn() :Int {
        return correcte
    }

    fun movimentsReturn() :Int {
        return moviments
    }


}