package com.example.memory

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Chronometer
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo


class EasyGameActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var c1: ImageView
    lateinit var c2: ImageView
    lateinit var c3: ImageView
    lateinit var c4: ImageView
    lateinit var c5: ImageView
    lateinit var c6: ImageView

    lateinit var time: Chronometer
    lateinit var textMovi: TextView

    private lateinit var easyViewModel: GameEasyViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.memoryball_easygame)

        easyViewModel = ViewModelProvider(this).get(GameEasyViewModel::class.java)

        actionBar?.hide()
        supportActionBar?.hide()

        c1 = findViewById(R.id.carta1)
        c2 = findViewById(R.id.carta2)
        c3 = findViewById(R.id.carta3)
        c4 = findViewById(R.id.carta4)
        c5 = findViewById(R.id.carta5)
        c6 = findViewById(R.id.carta6)
        textMovi = findViewById(R.id.moviments)
        time = findViewById(R.id.chronometer)

        c1.setOnClickListener(this)
        c2.setOnClickListener(this)
        c3.setOnClickListener(this)
        c4.setOnClickListener(this)
        c5.setOnClickListener(this)
        c6.setOnClickListener(this)

        updateUI()
        time.start()
    }

    override fun onClick(v: View?) {
        when (v) {
            c1 -> {easyViewModel.girarCarta(0)
                YoYo.with(Techniques.ZoomIn)
                    .duration(700)
                    .repeat(0)
                    .playOn(c1);
                updateUI()
                easyViewModel.moviment2()
                acabarJoc();
            }
            c2 -> {easyViewModel.girarCarta(1)
                YoYo.with(Techniques.ZoomIn)
                    .duration(700)
                    .repeat(0)
                    .playOn(c2);
                updateUI()
                easyViewModel.moviment2()
                acabarJoc();
            }
            c3 -> {easyViewModel.girarCarta(2)
                YoYo.with(Techniques.ZoomIn)
                    .duration(700)
                    .repeat(0)
                    .playOn(c3);
                updateUI()
                easyViewModel.moviment2()
                acabarJoc();
            }
            c4 -> {easyViewModel.girarCarta(3)
                YoYo.with(Techniques.ZoomIn)
                    .duration(700)
                    .repeat(0)
                    .playOn(c4);
                updateUI()
                easyViewModel.moviment2()
                acabarJoc();
            }
            c5 -> {easyViewModel.girarCarta(4)
                YoYo.with(Techniques.ZoomIn)
                    .duration(700)
                    .repeat(0)
                    .playOn(c5);
                updateUI()
                easyViewModel.moviment2()
                acabarJoc();
            }
            c6 -> {easyViewModel.girarCarta(5)
                YoYo.with(Techniques.ZoomIn)
                    .duration(700)
                    .repeat(0)
                    .playOn(c6);
                updateUI()
                easyViewModel.moviment2()
                acabarJoc();
            }
        }
    }


    private fun updateUI() {
        c1.setImageResource(easyViewModel.estatCarta(0))
        c2.setImageResource(easyViewModel.estatCarta(1))
        c3.setImageResource(easyViewModel.estatCarta(2))
        c4.setImageResource(easyViewModel.estatCarta(3))
        c5.setImageResource(easyViewModel.estatCarta(4))
        c6.setImageResource(easyViewModel.estatCarta(5))
    }

    fun comprobarPunts(): Int {
        val moviments = easyViewModel.movimentsReturn()
        var points = 0
        if (moviments < 5){
            points = 100
        }
        else if (moviments >= 5 && moviments < 7){
            points = 80
        }
        else if (moviments >= 7 && moviments < 10){
            points = 50
        }
        else{
            points = 0
        }
        return points;
    }

    fun acabarJoc(){
        val moviments = easyViewModel.movimentsReturn()
        val correcte = easyViewModel.correctesReturn()
        if (correcte == 3){
            time.stop()
            val intent = Intent(this, ResultActivity::class.java)
            val extras = Bundle()
            extras.putString("mode","Easy Mode")
            extras.putInt("punts",comprobarPunts())
            startActivity(intent.putExtras(extras))
        }
        textMovi.text = "Moviments: $moviments"
    }

}

