package com.example.memory

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget.Chronometer
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo

class DifGameActivity : AppCompatActivity(), OnClickListener {
    lateinit var c1: ImageView
    lateinit var c2: ImageView
    lateinit var c3: ImageView
    lateinit var c4: ImageView
    lateinit var c5: ImageView
    lateinit var c6: ImageView
    lateinit var c7: ImageView
    lateinit var c8: ImageView

    lateinit var time: Chronometer
    lateinit var textMovi: TextView

    private lateinit var difViewModel: GameDiffViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.memoryball_difgame)

        difViewModel = ViewModelProvider(this).get(GameDiffViewModel::class.java)

        actionBar?.hide()
        supportActionBar?.hide()

        c1 = findViewById(R.id.carta1)
        c2 = findViewById(R.id.carta2)
        c3 = findViewById(R.id.carta3)
        c4 = findViewById(R.id.carta4)
        c5 = findViewById(R.id.carta5)
        c6 = findViewById(R.id.carta6)
        c7 = findViewById(R.id.carta7)
        c8 = findViewById(R.id.carta8)
        textMovi = findViewById(R.id.moviments)
        time = findViewById(R.id.chronometer)


        c1.setOnClickListener(this)
        c2.setOnClickListener(this)
        c3.setOnClickListener(this)
        c4.setOnClickListener(this)
        c5.setOnClickListener(this)
        c6.setOnClickListener(this)
        c7.setOnClickListener(this)
        c8.setOnClickListener(this)

        updateUI()
        time.start()

    }


    override fun onClick(v: View?) {
        when (v) {
            c1 -> {difViewModel.girarCarta(0)
                YoYo.with(Techniques.Bounce)
                    .duration(700)
                    .repeat(0)
                    .playOn(c1);
                updateUI()
                difViewModel.moviment2()
                acabarJoc()
            }
            c2 -> {difViewModel.girarCarta(1)
                YoYo.with(Techniques.Bounce)
                    .duration(700)
                    .repeat(0)
                    .playOn(c2);
                updateUI()
                difViewModel.moviment2()
                acabarJoc()
            }
            c3 -> {difViewModel.girarCarta(2)
                YoYo.with(Techniques.Bounce)
                    .duration(700)
                    .repeat(0)
                    .playOn(c3);
                updateUI()
                difViewModel.moviment2()
                acabarJoc()
            }
            c4 -> {difViewModel.girarCarta(3)
                YoYo.with(Techniques.Bounce)
                    .duration(700)
                    .repeat(0)
                    .playOn(c4);
                updateUI()
                difViewModel.moviment2()
                acabarJoc()
            }
            c5 -> {difViewModel.girarCarta(4)
                YoYo.with(Techniques.Bounce)
                    .duration(700)
                    .repeat(0)
                    .playOn(c5);
                updateUI()
                difViewModel.moviment2()
                acabarJoc()
            }
            c6 -> {difViewModel.girarCarta(5)
                YoYo.with(Techniques.Bounce)
                    .duration(700)
                    .repeat(0)
                    .playOn(c6);
                updateUI()
                difViewModel.moviment2()
                acabarJoc()
            }
            c7 -> {difViewModel.girarCarta(6)
                YoYo.with(Techniques.Bounce)
                    .duration(700)
                    .repeat(0)
                    .playOn(c7);
                updateUI()
                difViewModel.moviment2()
                acabarJoc()
            }
            c8 -> {difViewModel.girarCarta(7)
                YoYo.with(Techniques.Bounce)
                    .duration(700)
                    .repeat(0)
                    .playOn(c8);
                updateUI()
                difViewModel.moviment2()
                acabarJoc()
            }
        }
    }

    private fun updateUI() {
        c1.setImageResource(difViewModel.estatCarta(0))
        c2.setImageResource(difViewModel.estatCarta(1))
        c3.setImageResource(difViewModel.estatCarta(2))
        c4.setImageResource(difViewModel.estatCarta(3))
        c5.setImageResource(difViewModel.estatCarta(4))
        c6.setImageResource(difViewModel.estatCarta(5))
        c7.setImageResource(difViewModel.estatCarta(6))
        c8.setImageResource(difViewModel.estatCarta(7))
    }

    fun comprobarPunts(): Int {
        val moviments = difViewModel.movimentsReturn()
        var points = 0
        if (moviments <= 6){
            points = 100
        }
        else if (moviments > 6 && moviments < 8){
            points = 80
        }
        else if (moviments >= 8 && moviments < 11){
            points = 50
        }
        else{
            points = 0
        }
        return points;
    }

    fun acabarJoc(){
        val moviments = difViewModel.movimentsReturn()
        val correcte = difViewModel.correctesReturn()
        if (correcte == 4){
            time.stop()
            val intent = Intent(this, ResultActivity::class.java)
            val extras = Bundle()
            extras.putString("mode","Difficult Mode")
            extras.putInt("punts",comprobarPunts())
            startActivity(intent.putExtras(extras))
        }
        textMovi.text = "Moviments: $moviments"
    }
}

