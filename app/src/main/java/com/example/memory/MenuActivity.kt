package com.example.memory

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.core.view.get
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import android.content.Intent

class MenuActivity : AppCompatActivity() {
    lateinit var rollPlay: Button
    lateinit var rollHelp: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.SplashTheme)
        setContentView(R.layout.memoryball_menu)
        actionBar?.hide()
        supportActionBar?.hide()
        rollPlay = findViewById(R.id.play)
        rollHelp = findViewById(R.id.help)
        val spinner: Spinner = findViewById(R.id.desplegable)

        rollPlay.setOnClickListener{
            if (spinner.selectedItemPosition == 0){
                val intent = Intent(this@MenuActivity, EasyGameActivity::class.java)
                startActivity(intent)
            }
            else if (spinner.selectedItemPosition == 1){
                val intent2 = Intent(this@MenuActivity, DifGameActivity::class.java)
                startActivity(intent2)
            }
        }

        rollHelp.setOnClickListener{
            MaterialAlertDialogBuilder(this)
                .setTitle(resources.getString(R.string.titleHelp))
                .setMessage(resources.getString(R.string.textHelp))
                .setNegativeButton(resources.getString(R.string.closeHelp)) { dialog, which ->
                    closeContextMenu()
                }
                .show()
        }

        ArrayAdapter.createFromResource(
            this,
            R.array.desplegable,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }


    }

}