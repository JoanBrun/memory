package com.example.memory

data class Carta(val id: Int, val resId: Int, var flipped: Boolean = false)
